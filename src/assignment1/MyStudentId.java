package assignment1;

public class MyStudentId {

	public static void main(String[] args) {
		int[] studentId = new int[] {1,2,3,4,5,6,7,8,9};
		int sum = 0;
		String zero = "zero";
		String two = "two";
		String four = "four";
		String six = "six";
		String eight = "eight";
		
		//loop through the array
		for(int i=0;i<studentId.length;i++) {
			int digit = studentId[i];
			System.out.print(digit + " ");
			sum = sum + digit;
			
			//if even ...
			if(digit % 2 == 0) {
				switch(digit) {
					case 0: 
						System.out.print(zero + " ");
						break;
					case 2: 
						System.out.print(two + " ");
						break;
					case 4: 
						System.out.print(four + " ");
						break;
					case 6: 
						System.out.print(six + " ");
						break;
					case 8: 
						System.out.print(eight + " ");
						break;
				}
			} else { //if it's not even, it's odd...
				for(int j=0;j<digit;j++) {
					System.out.print(digit);
				}
				System.out.print(" "); //whitespace
			}
			if(digit % 3 == 0) { //if digit divisible by 3
				System.out.print(sum); //we already have this value!
			}
			System.out.println(); //go to the next line.
		}
		System.out.println(sum);
	}
}
