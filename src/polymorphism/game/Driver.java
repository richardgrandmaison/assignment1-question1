package polymorphism.game;

public class Driver {

	public static void main(String[] args) {
		Player player = new Player("Richard", 10, 100);
		NormalEnemy enemy = new NormalEnemy("NormalEnemy", 7, 70);
		BossClass boss = new BossClass("Boss", 12, 200);
		
		player.normalAttack(enemy);
		enemy.specialPower(player);
		boss.normalAttack(player);
		player.specialPower(player);
		
		player.status();
		enemy.status();
		boss.status();	
	}

}
