package polymorphism.game;

public abstract class Unit {
	
	public int attackPower;
	public int healthPoints;
	public String name;
	
	public Unit(String name, int attackPower, int healthPoints) {
		this.name = name;
		this.attackPower = attackPower;
		this.healthPoints = healthPoints;
	}
	
	public void status() {
		System.out.println(this.name + " has " + healthPoints + "hp and has " + 
				attackPower + " attackPower.");
	}
	
	public void normalAttack(Unit enemy) {
		System.out.println(this.name + " attacks " + enemy.name + " for " + attackPower + "!");
		enemy.healthPoints -= this.attackPower;
	}
	
	public abstract void specialPower(Unit unit);
}
