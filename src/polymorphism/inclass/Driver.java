package polymorphism.inclass;

public class Driver {

	public static void main(String[] args) {
		Animal animal = new Animal();
		Dog dog = new Dog();
		
		dog.changeFood("bones");
		dog.changeType("dog");
		
		animal.eat();
		animal.move();
		animal.speak();
		dog.eat();
		dog.move();
		dog.speak();
		
	
	}

}
