package polymorphism.inclass;

public class Animal {
	
	private String type="animal";
	private String food="food";
	
	protected final void changeType(String type) {
		this.type = type;
	}
	protected final void changeFood(String food) {
		this.food = food;
	}
	protected final String getType() {
		return this.type;
	}
	protected final String getFood() {
		return this.food;
	}
	
	protected void speak() {
		System.out.println("My type is: " + this.type);
	}
	protected void eat() {
		System.out.println("I eat " + this.food);
	}
	protected void move() {
		System.out.println("I can move");
	}
}
